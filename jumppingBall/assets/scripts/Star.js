// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        pickRadius:0
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    //获取 star 和 player 的距离
    getDistanceWithPlayer()
    {
        return this.node.position.sub(this.gameControl.player.position).mag()
    },
    onPick()
    {
        this.gameControl.makeRandomStar()
        this.node.destroy()
    },
    start () {

    },

    update (dt) {
        let opacityRatio = 1 - this.gameControl.time/this.gameControl.starShowTime;
        let minOpacity = 0;
        this.node.opacity = minOpacity + Math.floor(opacityRatio * (255 - minOpacity));
        if(this.getDistanceWithPlayer() < this.pickRadius )
        {
            this.gameControl.gainScore()
            this.onPick()
        }
    },
});
