// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        //跳跃的高度
        jumpHeight:{
            default:100,
            type:cc.Integer
        },
        //跳跃的时间间隔
        jumpDuration:{
            default:0.8,
            type:cc.Float
        },
        //水平方向最大移动速度
        maxSpeed:{
            default:100,
            type:cc.Integer
        },
        //按下AD按钮 加速度
        accel:{
            default:10,
            type:cc.Integer
        },
        playJumpSound:{
            default:null,
            type:cc.AudioClip
        }

    },

    // LIFE-CYCLE CALLBACKS:

    palyJumpAudio:function()
    {
        cc.audioEngine.playEffect(this.playJumpSound,false)
    },
    setJumpAction:function()
    {
        let jumpUp = cc.moveBy(this.jumpDuration,cc.v2(0,this.jumpHeight)).easing(cc.easeCubicActionInOut())
        let jumpDown = cc.moveBy(this.jumpDuration,cc.v2(0,-this.jumpHeight)).easing(cc.easeCubicActionInOut())
        let playJumpSound = cc.callFunc(this.palyJumpAudio,this)
        return cc.repeatForever(cc.sequence(jumpUp,jumpDown,playJumpSound))
    },
    onKeyDown:function(event)
    {
        if (event.keyCode == cc.macro.KEY.a)
        {
            this.acceLL = true
        }else if (event.keyCode == cc.macro.KEY.d)
        {
            this.acceLR = true
        }
    },
    onKeyUp:function(event)
    {
        if (event.keyCode == cc.macro.KEY.a)
        {
            this.acceLL = false
        }else if (event.keyCode == cc.macro.KEY.d)
        {
            this.acceLR = false
        }
    },
    //重新开始跳动
    startMoveAt(pos)
    {   
        this.enabled = true
        this.speedX = 0
        this.node.setPosition(pos)
        this.node.runAction(this.setJumpAction())  
    },
    stopMove()
    {     
        this.enabled = false
        this.node.stopAllActions()
    },
    onLoad () {
        // let actions = this.setJumpAction()
        // this.node.runAction(actions)
        this.nodeParent = this.node.parent
        this.acceLL = false
        this.acceLR = false
        this.speedX = 0 
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP,this.onKeyUp,this)
    },
    start () {
       
    },


    update (dt) {
        if(this.acceLL)
        {
            this.speedX = this.speedX - this.accel*dt
        }
        else if (this.acceLR)
        {
            this.speedX = this.speedX + this.accel*dt
        }
        if (Math.abs(this.speedX) > this.maxSpeed)
        {
            this.speedX = this.speedX/Math.abs(this.speedX) * this.maxSpeed
        }
        // console.log("当前的速度------------- ",this.speedX)

        this.node.x += this.speedX*dt
        if(this.node.x > this.nodeParent.width/2 - this.node.width/2)
        {
            this.node.x = this.nodeParent.width/2 -this.node.width/2
            this.speedX = 0
        }else if (this.node.x < - this.nodeParent.width/2 +this.node.width/2)
        {
            this.node.x = - this.nodeParent.width/2 + this.node.width/2
            this.speedX = 0 
        }
    },

    onDestroy:function()
    {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this)
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP,this.onKeyUp,this)
    }
});
