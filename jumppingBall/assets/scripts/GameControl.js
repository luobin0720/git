// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        btnPlay: {
            default: null,
            type: cc.Button
        },
        starPrefab: {
            default: null,
            type: cc.Prefab
        },
        minStarShowTime:
        {
            default: 0,
            type: cc.Float
        },
        maxStarShowTime:
        {
            default: 0,
            type: cc.Float
        },
        player: {
            default: null,
            type: cc.Node
        },
        bgGround: {
            default: null,
            type: cc.Node
        },
        scoreLabel: {
            default: null,
            type: cc.Label
        },
        //资源部分
        scoreSound: {
            default: null,
            type: cc.AudioClip
        }
    },
    //得分
    gainScore() {
        this.score += 1
        this.scoreLabel.string = "Score:" + this.score
    },
    //重置得分
    resetScore()
    {
        this.score = 0
        this.scoreLabel.string = "Score:0"

    },
    //获取星星随机位置
    getRandomPosition() {
        let playerHeight = this.player.getComponent("Player").jumpHeight
        let starPosY = this.groundY + Math.random() * playerHeight + 30
        let starPosX = (this.node.width - this.nowStar.width)* (Math.random() - 0.5)
        return cc.v2(starPosX, starPosY)
    },
    //生成随机位置的星星
    makeRandomStar() {
        //重置时间
        this.time = 0
        this.starShowTime = this.minStarShowTime + Math.random() * (this.maxStarShowTime - this.minStarShowTime)
        this.nowStar = cc.instantiate(this.starPrefab)
        let randomPos = this.getRandomPosition()
        this.node.addChild(this.nowStar)
        //添加star 对GameControl的引用
        this.nowStar.getComponent("Star").gameControl = this
        this.nowStar.setPosition(randomPos)
    },
    //游戏结束
    gameOver() {
        this.btnPlay.node.active = true
        this.player.getComponent("Player").stopMove()
        this.nowStar.destroy()
    },
    gameStart() {
        this.enabled = true
        this.resetScore()
        this.player.getComponent("Player").startMoveAt(cc.v2(0,this.groundY))
        this.btnPlay.node.active = false      
        this.makeRandomStar()
    },
    onLoad() {
        // this.enabled= true
        //是否为第一次启动
        this.isFirst = true
        //时间
        this.time = 0
        //星星渐变时间
        this.starShowTme = 0

        //得分
        this.score = 0
        this.groundY = this.bgGround.y + this.bgGround.height / 2

    },

    start() {
        
    },

    update(dt) {
        //每帧判断是否人物与星星 碰撞检测
        if (this.time >= this.starShowTime) {
            this.gameOver()
            this.enabled = false
            return 
        }
        this.time += dt

    },
});
